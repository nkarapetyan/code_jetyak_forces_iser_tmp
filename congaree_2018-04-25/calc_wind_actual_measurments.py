import pandas as pd
import numpy as np

#----------------------------------------------------------------------------------------
# Calculating the actual wind speed and the direction
#----------------------------------------------------------------------------------------

def calc_actual_wind_speed_and_dir(df):
    WIND_SENSOR_SPEED_LABEL = "wind_sensor_speed"
    WIND_SENSOR_DIR_LABEL = "wind_sensor_dir"
    BOAT_HEADING_LABEL = "boat_heading"
    BOAT_LINEAR_X_LABEL = "boat_linear_x"
    BOAT_LINEAR_Y_LABEL = "boat_linear_y"
    
    wind_sensor_speed = df[WIND_SENSOR_SPEED_LABEL] # m/s
    wind_sensor_dir = np.radians(df[WIND_SENSOR_DIR_LABEL]) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt boat reference frame
    
    boat_heading = np.radians(df[BOAT_HEADING_LABEL]) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt World reference frame
    boat_speed_linear_x = df[BOAT_LINEAR_X_LABEL] # wrt boat reference frame TODO check
    boat_speed_linear_y = df[BOAT_LINEAR_Y_LABEL] # wrt boat reference frame TODO check
    
    #Some pre-processing to make knowns available in the correct format
    #Convert Wind Sensor Direction to World Reference Frame and convert to radians, ANGLEr
    wind_sensor_dir_world = ((boat_heading+wind_sensor_dir)%(2*np.pi)) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt World reference frame
    
    # Transform the world reference frame to become from N (0) - E (pi/2) - W (3/2pi) - S (pi) to N (0) - E (3/2pi) - W (pi/2) - S (pi)
    #Calc polar boat speed from x and y linear components, b
    boat_speed_p = np.sqrt(np.power(boat_speed_linear_x, 2)+np.power(boat_speed_linear_y, 2))
    #at this speed and boat heading calculate boat cartesian coordinates
    boat_speed_x = np.abs(boat_speed_p) * np.cos(boat_heading)
    boat_speed_y = -np.abs(boat_speed_p) * np.sin(boat_heading) # invert y axis to invert E and W angle 
    #convert boat speed angle back to polar for Calculating Angle of real wind in system of equations
    boat_heading_p = np.arctan2(boat_speed_y, boat_speed_x)
    
    # Same calculations for the wind sensor.
    #Calc x and y components of wind sensor velocity measurments Rx, Ry
    wind_sensor_speed_x = np.abs(wind_sensor_speed) * np.cos(wind_sensor_dir_world)
    wind_sensor_speed_y = -np.abs(wind_sensor_speed) * np.sin(wind_sensor_dir_world) #flipped cartesian y value
    wind_sensor_dir_p = np.arctan2(wind_sensor_speed_y, wind_sensor_speed_x)
    
    #Calculate the actual wind vector, starting from the following equation: wind_sensor = boat + actual_wind
    #ANGLEw (wind direction in world ref frame)
    actual_wind_dir = np.pi + np.arctan2((wind_sensor_speed_y - boat_speed_p * np.sin(boat_heading_p)), (boat_speed_p * np.cos(boat_heading_p) - wind_sensor_speed_x))
    actual_wind_speed = np.abs((boat_speed_p * np.cos(boat_heading_p) - wind_sensor_speed_x) / np.cos(actual_wind_dir))

    return actual_wind_speed, actual_wind_dir
#--------------------------------------------------------------------------------------------------------------------------------
