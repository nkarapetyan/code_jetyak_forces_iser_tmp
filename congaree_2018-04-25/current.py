import matplotlib
import matplotlib.pyplot as plt

#from mpl_toolkits.basemap import Basemap
from mpl_toolkits.mplot3d import axes3d

import numpy as np
from numpy import ma

import pandas as pd
from pandas import DataFrame

import GPy

FONTSIZE = 30

# INPUT TO MODIFY
#filename = 'data/congaree_2018-04-25.csv' # use this for whole data set
#filename = 'data/congaree_2018-04-25_depth_cleaned.csv' #use this for depth calcs omitting out of range.
filename = 'data/congaree_2018-04-25_current_cleaned.csv' #use this for current calcs omitting 0 readings.
MEASUREMENT_STEP = 4 # Take measurements every MEASUREMENT_STEP
#1-1249: launching; 1250-2019: Dock parallel current sensors; 2020-2299: dock x-paralled, y-perpendicular; 2649-3059: autonomous wp mission single parraled; 4535-4630: auto perpendicular y, parralel x
SEQUENCE_MIN = 2649
SEQUENCE_MAX = 3059
TRAINING_DATA_RULE = "[0:len(pattern):MEASUREMENT_STEP]"
GRID_LATITUDE_NUM_CELLS = 50
GRID_LONGITUDE_NUM_CELLS = 50
VARIANCE_RBF_CURRENT_SPEED = 0.1
LENGTHSCALE_RBF_CURRENT_SPEED = 0.00001
VARIANCE_RBF_CURRENT_DIR = 0.1
LENGTHSCALE_RBF_CURRENT_DIR = 0.00001
DOMINANT_CURRENT_DIRECTION = 1.97222
# END INPUT TO MODIFY

# CONSTANTS
# Names of the fields in the CSV file
SEQUENCE_STRING = "seq"
LATITUDE_STRING = "Latitude"
LONGITUDE_STRING = "Longitude"
CURRENT_SENSOR_1_STRING = "current_x"
CURRENT_SENSOR_2_STRING = "current_y"
COLORBAR_FORMAT = '%1.6f' # https://stackoverflow.com/questions/42153735/disabling-scientific-notation-of-imshow-colorbar-in-matplotlib
# END CONSTANTS

# DO NOT MODIFY UNLESS YOU KNOW WHAT YOU'RE DOING

# Read in data from CSV
readcsv = pd.read_csv(filename)
#Change sequence bounds to filter whole csv file for areas of interest.
pattern = readcsv.loc[(readcsv[SEQUENCE_STRING] > SEQUENCE_MIN) & (readcsv[SEQUENCE_STRING] < SEQUENCE_MAX)]
#pattern, test_data = train_test_split(readcsv, test_size = 0.25)

# Variables storing info from the CSV file
timestamp = readcsv.Timestamp
lat = pattern[LATITUDE_STRING]
lon = pattern[LONGITUDE_STRING]
current_sensor_1_ticks = pattern[CURRENT_SENSOR_1_STRING] #raw read in ticks
current_sensor_2_ticks = pattern[CURRENT_SENSOR_2_STRING] #raw read in ticks

#convert current sensor ticks to m/s speeds mult by calibration factor 1.0977126
current_sensor_1_reading = (current_sensor_1_ticks*.51444444*1.0977126)/4.8 
current_sensor_2_reading = (current_sensor_2_ticks*.51444444*1.0977126)/4.8 

#convert boat heading in degrees to radians and read linear velocities
boat_heading = np.radians(pattern.boat_heading) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt World reference frame
current_sensor_dir_world = boat_heading

boat_speed_linear_x = pattern.boat_linear_x # wrt boat reference frame TODO check
boat_speed_linear_y = pattern.boat_linear_y # wrt boat reference frame TODO check

# Transform the world reference frame to become from N (0) - E (pi/2) - W (3/2pi) - S (pi) to N (0) - E (3/2pi) - W (pi/2) - S (pi)
#Calc polar boat speed from x and y linear components
boat_speed_p = np.sqrt(np.power(boat_speed_linear_x, 2)+np.power(boat_speed_linear_y, 2))

# Some calculations for the momentary measurements of current sensors 1 & 2 
current_speed_1 = current_sensor_1_reading
current_speed_2 = current_sensor_2_reading
average_current_speed = (current_speed_1 +current_speed_2)/2

print np.average(average_current_speed)

#calculate actual current speed when sensor orientation is valid between pi/6 and 11pi/6 or 5pi/6 and 7pi/6
#if ((boat_heading < np.pi/6 and boat_heading > 11*np.pi/6) or (boat_heading < 7*np.pi/6 and boat_heading > 5*np.pi/6)):
actual_current_speed = (average_current_speed - boat_speed_p)/(np.cos(boat_heading - DOMINANT_CURRENT_DIRECTION))

#Calc x and y components of current sensors velocity measurments Rx, Ry
average_momentary_current_speed_x = (average_current_speed) * np.cos(current_sensor_dir_world)
average_momentary_current_speed_y = (average_current_speed) * np.sin(current_sensor_dir_world) #may need to flip

######################################################################################
#actual_current_dir = DOMINANT_CURRENT_DIRECTION

actual_current_dir =  boat_heading+DOMINANT_CURRENT_DIRECTION-boat_heading

actual_current_speed_x = np.abs(actual_current_speed) * np.cos(actual_current_dir+np.pi) # TODO More general, from the beginning check the direction.
actual_current_speed_y = np.abs(actual_current_speed) * np.sin(actual_current_dir+np.pi)
#print (actual_current_speed)

# Finished the calculation.

exec('X = pd.concat([lon' + TRAINING_DATA_RULE + ', lat' + TRAINING_DATA_RULE + '], axis=1).values')
exec('W = pd.concat([actual_current_dir' + TRAINING_DATA_RULE + ', actual_current_speed' + TRAINING_DATA_RULE + '], axis=1).values')

#END scikit


#GPy
# Kernel creation
# RBF Kernel def
#gp_kernel_current_dir = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_CURRENT_DIR, lengthscale=LENGTHSCALE_RBF_CURRENT_DIR)
#gp_kernel_current_speed = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_CURRENT_SPEED, lengthscale=LENGTHSCALE_RBF_CURRENT_SPEED)

#Matern32 Kernel def
gp_kernel_current_speed = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_CURRENT_SPEED, lengthscale=LENGTHSCALE_RBF_CURRENT_SPEED)
gp_kernel_current_dir = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_CURRENT_DIR, lengthscale=LENGTHSCALE_RBF_CURRENT_DIR)

currentDirGprMdl = GPy.models.GPRegression(X, np.reshape(W[:,0],(len(W[:,0]),1)),gp_kernel_current_dir, noise_var=0.1 )
currentSpeedGprMdl = GPy.models.GPRegression(X,  np.reshape(W[:,1],(len(W[:,1]),1)),gp_kernel_current_speed, noise_var=0.1 )
#END GPy

#Test set over a grid.
# Create the grid and the test dataset.
grid_longitude = np.linspace(min(X[:,0]), max(X[:,0]), GRID_LONGITUDE_NUM_CELLS) #50 jm
grid_latitude = np.linspace(min(X[:,1]), max(X[:,1]), GRID_LATITUDE_NUM_CELLS) #50 jm

LON, LAT = np.meshgrid(grid_longitude, grid_latitude)
Xtest = np.array([LON.flatten(), LAT.flatten()]).T

# GP Prediction

#GPy
currentDirGprMdl.optimize_restarts(num_restarts=5, messages=True)
currentSpeedGprMdl.optimize_restarts(num_restarts=5, messages=True)

currentDirPred, currentDirPredsigma = currentDirGprMdl.predict(Xtest)
currentSpeedPred, cspdsigma = currentSpeedGprMdl.predict(Xtest)
#END GPy

# Plot
# Reshaping predictions in a grid form
grid_current_dir_pred = np.reshape(currentDirPred, LAT.shape);
currentDirPredsigma = np.reshape(currentDirPredsigma, LAT.shape)

grid_current_speed_pred = np.reshape(currentSpeedPred, LAT.shape);
cspdsigma = np.reshape(cspdsigma, LAT.shape)

# Calculate x and y components of the predicted current speed.
grid_current_x_vector = -np.abs(np.multiply(grid_current_speed_pred, np.cos(grid_current_dir_pred)))
grid_current_y_vector = -np.abs(np.multiply(grid_current_speed_pred, np.sin(grid_current_dir_pred)))

#print (actual_current_speed)
#prepare uncertainty for instensity coloring on plot

plt.figure()
exec('plt.quiver(lon' + TRAINING_DATA_RULE + ', lat' + TRAINING_DATA_RULE + ', actual_current_speed_x' + TRAINING_DATA_RULE + ',  actual_current_speed_y' + TRAINING_DATA_RULE + ')')

plt.plot(X[:,0], X[:,1], 'r+')
plt.title("Actual Current Measurements", fontsize=FONTSIZE)
plt.ticklabel_format(useOffset=False)
xlabel = "Longitude"
ylabel = "Latitude"
plt.xlabel(xlabel, fontsize=FONTSIZE)
plt.ylabel(ylabel, labelpad = 15, fontsize=FONTSIZE)
plt.subplots_adjust(left=0.2)
ax = plt.gca()
ax.tick_params(axis = 'both', which = 'major', labelsize=FONTSIZE)
ax.tick_params(axis = 'both', which = 'minor', labelsize=FONTSIZE)
"""
plt.figure()
plt.quiver(LON, LAT, grid_current_x_vector, grid_current_y_vector)
plt.plot(X[:,0], X[:,1], 'r+')
plt.title("Current Intensity Map Grid")
plt.ticklabel_format(useOffset=False)
"""

def plot_contour(x, y, z, title, measurements=None):
    """  Plot contour with measurement points.
    Args:
        x: 1d vector
        y: 1d vector
        z: 1d vector
        measurements: 2d vector
        title: 1d vector
    """
    plt.figure()
    plt.contourf(x, y, z,linewidth=0)
    #if measurements is not None:
    #    plt.plot(measurements[:,0], measurements[:,1], 'r+')
    cbar = plt.colorbar(format = COLORBAR_FORMAT)
    plt.title(title, fontsize=FONTSIZE)
    plt.ticklabel_format(useOffset=False)
    ax = plt.gca()
    ax.tick_params(axis = 'both', which = 'major', labelsize=FONTSIZE)
    ax.tick_params(axis = 'both', which = 'minor', labelsize=FONTSIZE)
    cbar.ax.tick_params(labelsize=FONTSIZE)

'''
plot_contour(LON, LAT, grid_current_speed_pred, "Current Speed Prediction", X)

plot_contour(LON, LAT, cspdsigma, "Current Speed Uncertainty", X)

plot_contour(LON, LAT, grid_current_dir_pred, "Current Direction Prediction", X)

plot_contour(LON, LAT, currentDirPredsigma, "Current Direction Uncertainty", X)
'''

plt.show()
