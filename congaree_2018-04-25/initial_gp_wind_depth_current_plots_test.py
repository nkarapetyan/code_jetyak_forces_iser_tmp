import matplotlib
import matplotlib.pyplot as plt

#from mpl_toolkits.basemap import Basemap
from mpl_toolkits.mplot3d import axes3d

import numpy as np
from numpy import ma

import pandas as pd
from pandas import DataFrame

#jmfrom sklearn.model_selection import train_test_split #FIXME:N added

#from sklearn.gaussian_process import GaussianProcessRegressor
#from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, WhiteKernel, ExpSineSquared
import GPy

#Grid search parallel to wind
#SEQUENCE_MIN = 15500
#SEQUENCE_MAX = 17000
#Grid search perpendicular to wind
#SEQUENCE_MIN = 18000
#SEQUENCE_MAX = 20000
#Star search pattern
#SEQUENCE_MIN = 20500
#SEQUENCE_MAX = 21750
#Linear training line
#SEQUENCE_MIN = 23200
#SEQUENCE_MAX = 23500
# INPUT TO MODIFY
#filename = 'data/congaree_2018-04-25.csv' # use this for whole data set
#filename = 'data/congaree_2018-04-25_depth_cleaned.csv' #use this for depth calcs omitting out of range.
filename = 'data/congaree_2018-04-25_current_cleaned.csv' #use this for current calcs omitting 0 readings.
MEASUREMENT_STEP = 4 # Take measurements every MEASUREMENT_STEP
#1-1249: launching; 1250-2019: Dock parallel current sensors; 2020-2299: dock x-paralled, y-perpendicular; 2649-3059: autonomous wp mission single parraled; 4535-4630: auto perpendicular y, parralel x
SEQUENCE_MIN = 2649
SEQUENCE_MAX = 3059
TRAINING_DATA_RULE = "[0:len(pattern):MEASUREMENT_STEP]"
GRID_LATITUDE_NUM_CELLS = 50
GRID_LONGITUDE_NUM_CELLS = 50
VARIANCE_RBF_WIND_DIR = 0.1
LENGTHSCALE_RBF_WIND_DIR = 0.00001
VARIANCE_RBF_WIND_SPEED = 0.1
LENGTHSCALE_RBF_WIND_SPEED = 0.00001
VARIANCE_RBF_DEPTH = 0.1
LENGTHSCALE_RBF_DEPTH = 0.00001
VARIANCE_RBF_CURRENT_SPEED = 0.1
LENGTHSCALE_RBF_CURRENT_SPEED = 0.00001
VARIANCE_RBF_CURRENT_DIR = 0.1
LENGTHSCALE_RBF_CURRENT_DIR = 0.00001


# END INPUT TO MODIFY

# CONSTANTS
# Names of the fields in the CSV file
SEQUENCE_STRING = "seq"
LATITUDE_STRING = "Latitude"
LONGITUDE_STRING = "Longitude"
CURRENT_SENSOR_1_STRING = "current_x"
CURRENT_SENSOR_2_STRING = "current_y"
DEPTH_SENSOR_STRING = "depth"
WIND_SENSOR_SPEED_STRING = "wind_sensor_speed"
WIND_SENSOR_DIR_STRING = "wind_sensor_dir"
COLORBAR_FORMAT = '%1.6f' # https://stackoverflow.com/questions/42153735/disabling-scientific-notation-of-imshow-colorbar-in-matplotlib
# END CONSTANTS

# DO NOT MODIFY UNLESS YOU KNOW WHAT YOU'RE DOING

# Read in data from CSV
readcsv = pd.read_csv(filename)
#Change sequence bounds to filter whole csv file for areas of interest.
pattern = readcsv.loc[(readcsv[SEQUENCE_STRING] > SEQUENCE_MIN) & (readcsv[SEQUENCE_STRING] < SEQUENCE_MAX)]
#pattern, test_data = train_test_split(readcsv, test_size = 0.25)

"""
# TODO do we want a filtering according to the region? if yes, then we should use the same way that we are filtering with SEQUENCE_STRING
# Create the training dataset
# Limits of the region
LATITUDE_MIN = min(lat) #34.130952;
LATITUDE_MAX = max(lat) #34.131755;
LONGITUDE_MIN = min(lon) #-81.288802; 
LONGITUDE_MAX = max(lon) #-81.287947;


# Get indices of consecutive measurements within the region.
indices_latitude = np.where(np.logical_and(lat > LATITUDE_MIN, lat < LATITUDE_MAX))[0] 
indices_longitude = np.where(np.logical_and(lon > LONGITUDE_MIN, lon < LONGITUDE_MAX))[0]

if len(indices_latitude) > len(indices_longitude):
    indices = indices_longitude;
else:
    indices = indices_latitude;

# TODO Do we still want to avoid jumps?
indices = np.where(np.diff(indices)==1)[0] # TODO possibly change to get a single transect
"""
# Now the data is sliced in the region and sequence that we wanted.
# Here calculation of the wind vector, according to the transformations wrt boat and wind sensor measurement.

# Variables storing info from the CSV file
timestamp = readcsv.Timestamp

lat = pattern[LATITUDE_STRING]
lon = pattern[LONGITUDE_STRING]
current_sensor_1_ticks = pattern[CURRENT_SENSOR_1_STRING] #raw read in ticks
current_sensor_2_ticks = pattern[CURRENT_SENSOR_2_STRING] #raw read in ticks
depth_sensor_reading = pattern[DEPTH_SENSOR_STRING]
wind_sensor_speed = pattern[WIND_SENSOR_SPEED_STRING] # m/s
wind_sensor_dir = np.radians(pattern[WIND_SENSOR_DIR_STRING]) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt boat reference frame

#convert current sensor ticks to m/s speeds
current_sensor_1_reading = (current_sensor_1_ticks*.51444444*1.0977126)/4.8 
current_sensor_2_reading = (current_sensor_2_ticks*.51444444*1.0977126)/4.8 

#convert boat heading in degrees to radians and read linear velocities
boat_heading = np.radians(pattern.boat_heading) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt World reference frame
boat_speed_linear_x = pattern.boat_linear_x # wrt boat reference frame TODO check
boat_speed_linear_y = pattern.boat_linear_y # wrt boat reference frame TODO check

#Some pre-processing to make knowns available in the correct format
#Convert Sensor Direction to World Reference Frame and convert to radians, ANGLEr
wind_sensor_dir_world = ((boat_heading+wind_sensor_dir)%(2*np.pi)) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt World reference frame
current_sensor_dir_world = boat_heading

# Transform the world reference frame to become from N (0) - E (pi/2) - W (3/2pi) - S (pi) to N (0) - E (3/2pi) - W (pi/2) - S (pi)
#Calc polar boat speed from x and y linear components, b
boat_speed_p = np.sqrt(np.power(boat_speed_linear_x, 2)+np.power(boat_speed_linear_y, 2))
#at this speed and boat heading calculate boat cartesian coordinates
boat_speed_x = np.abs(boat_speed_p) * np.cos(boat_heading)
boat_speed_y = -np.abs(boat_speed_p) * np.sin(boat_heading) # invert y axis to invert E and W angle 
#convert boat speed angle back to polar for Calculating Angle of real wind in system of equations
boat_heading_p = np.arctan2(boat_speed_y, boat_speed_x)

# Same calculations for the wind sensor.
#Calc x and y components of wind sensor velocity measurments Rx, Ry
wind_sensor_speed_x = np.abs(wind_sensor_speed) * np.cos(wind_sensor_dir_world)
wind_sensor_speed_y = -np.abs(wind_sensor_speed) * np.sin(wind_sensor_dir_world) #flipped cartesian y value
wind_sensor_dir_p = np.arctan2(wind_sensor_speed_y, wind_sensor_speed_x)

# Some calculations for the momentary measurements of current sensors 1 & 2 
current_speed_1 = np.abs(boat_speed_p - current_sensor_1_reading)
current_speed_2 = np.abs(boat_speed_p - current_sensor_2_reading)
average_current_speed = (current_speed_1 +current_speed_2)/2

#Calc x and y components of current sensors velocity measurments Rx, Ry
average_momentary_current_speed_x = (average_current_speed) * np.cos(current_sensor_dir_world)
average_momentary_current_speed_y = (average_current_speed) * np.sin(current_sensor_dir_world) #may need to flip

#Calculate the actual wind vector, starting from the following equation: wind_sensor = boat + actual_wind
#ANGLEw (wind direction in world ref frame)
actual_wind_dir = np.pi + np.arctan2((wind_sensor_speed_y - boat_speed_p * np.sin(boat_heading_p)), (boat_speed_p * np.cos(boat_heading_p) - wind_sensor_speed_x))
actual_wind_speed = np.abs((boat_speed_p * np.cos(boat_heading_p) - wind_sensor_speed_x) / np.cos(actual_wind_dir))
actual_wind_speed_x = np.abs(actual_wind_speed) * np.cos(actual_wind_dir)
actual_wind_speed_y = np.abs(actual_wind_speed) * np.sin(actual_wind_dir)

######################################################################################
#Calculate the actual current vector, starting from the following equation: current = (somerelation)(current1+current2)
#ANGLEw (current direction in world ref frame)
actual_current_dir =  np.pi/2 - np.arctan2((average_momentary_current_speed_y - boat_speed_p * np.sin(boat_heading_p)), (boat_speed_p * np.cos(boat_heading_p) - average_momentary_current_speed_x)) #np.pi +
actual_current_speed = np.abs((boat_speed_p * np.cos(boat_heading_p) - average_momentary_current_speed_x) / np.cos(actual_current_dir))
actual_current_speed_x = np.abs(actual_current_speed) * np.cos(actual_current_dir)
actual_current_speed_y = np.abs(actual_current_speed) * np.sin(actual_current_dir)
#print (actual_current_speed)

# Finished the calculation.

#Assign Input Output Variables for GP training data
#X = pd.concat([lon[0:len(pattern):MEASUREMENT_STEP], lat[0:len(pattern):MEASUREMENT_STEP]], axis=1).values
#Y = pd.concat([actual_wind_dir[0:len(pattern):MEASUREMENT_STEP], actual_wind_speed[0:len(pattern):MEASUREMENT_STEP]], axis=1).values

exec('X = pd.concat([lon' + TRAINING_DATA_RULE + ', lat' + TRAINING_DATA_RULE + '], axis=1).values')
exec('Y = pd.concat([actual_wind_dir' + TRAINING_DATA_RULE + ', actual_wind_speed' + TRAINING_DATA_RULE + '], axis=1).values')
exec('Z = pd.concat([depth_sensor_reading' + TRAINING_DATA_RULE + '], axis=1).values')
exec('W = pd.concat([actual_current_dir' + TRAINING_DATA_RULE + ', actual_current_speed' + TRAINING_DATA_RULE + '], axis=1).values')

# Instanciate a Gaussian Process model
#scikit # TODO choice between different libraries
#gp_kernel = ExpSineSquared(1.0, 5.0, periodicity_bounds=(1e-2, 1e1)) \
#    + WhiteKernel(1e-1)
#gpr = GaussianProcessRegressor(kernel=gp_kernel)

#gp_kernel = C(1.0, (1e-3, 1e3)) * RBF(10, (1e-2, 1e2))
"""
gp_kernel = RBF(100)
windDirGprMdl = GaussianProcessRegressor(kernel=gp_kernel, n_restarts_optimizer=9)
windSpeedGprMdl = GaussianProcessRegressor(kernel=gp_kernel, n_restarts_optimizer=9)
# Fit to data using Maximum Likelihood Estimation of the parameters
windDirGprMdl.fit(X, Y[:,0])
windSpeedGprMdl.fit(X, Y[:,1])
"""
#END scikit


#GPy
# Kernel creation
# RBF Kernel def
#gp_kernel_dir = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_WIND_DIR, lengthscale=LENGTHSCALE_RBF_WIND_DIR)
#gp_kernel_speed = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_WIND_SPEED, lengthscale=LENGTHSCALE_RBF_WIND_SPEED)
#gp_kernel_current_dir = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_CURRENT_DIR, lengthscale=LENGTHSCALE_RBF_CURRENT_DIR)
#gp_kernel_current_speed = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_CURRENT_SPEED, lengthscale=LENGTHSCALE_RBF_CURRENT_SPEED)
gp_kernel_depth = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_DEPTH, lengthscale=LENGTHSCALE_RBF_DEPTH)

#Matern32 Kernel def
gp_kernel_speed = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_WIND_SPEED, lengthscale=LENGTHSCALE_RBF_WIND_SPEED)
gp_kernel_dir = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_WIND_DIR, lengthscale=LENGTHSCALE_RBF_WIND_DIR)
gp_kernel_current_speed = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_CURRENT_SPEED, lengthscale=LENGTHSCALE_RBF_CURRENT_SPEED)
gp_kernel_current_dir = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_CURRENT_DIR, lengthscale=LENGTHSCALE_RBF_CURRENT_DIR)
#gp_kernel_depth = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_DEPTH, lengthscale=LENGTHSCALE_RBF_DEPTH)

windDirGprMdl = GPy.models.GPRegression(X, np.reshape(Y[:,0],(len(Y[:,0]),1)),gp_kernel_dir, noise_var=0.1 )
windSpeedGprMdl = GPy.models.GPRegression(X,  np.reshape(Y[:,1],(len(Y[:,1]),1)),gp_kernel_speed, noise_var=0.1 )
currentDirGprMdl = GPy.models.GPRegression(X, np.reshape(W[:,0],(len(W[:,0]),1)),gp_kernel_current_dir, noise_var=0.1 )
currentSpeedGprMdl = GPy.models.GPRegression(X,  np.reshape(W[:,1],(len(W[:,1]),1)),gp_kernel_current_speed, noise_var=0.1 )
depthGprMdl = GPy.models.GPRegression(X, np.reshape(Z[:,0],(len(Z[:,0]),1)),gp_kernel_depth, noise_var=0.1 )
#END GPy

#Test set over a grid.
# Create the grid and the test dataset.
grid_longitude = np.linspace(min(X[:,0]), max(X[:,0]), GRID_LONGITUDE_NUM_CELLS) #50 jm
grid_latitude = np.linspace(min(X[:,1]), max(X[:,1]), GRID_LATITUDE_NUM_CELLS) #50 jm

LON, LAT = np.meshgrid(grid_longitude, grid_latitude)
Xtest = np.array([LON.flatten(), LAT.flatten()]).T

# GP Prediction
#scikit
"""
windDirPred, windDirPredsigma = windDirGprMdl.predict(Xtest, return_std=True) #gp.predict(x, return_std=True)
windSpeedPred, wspdsigma = windSpeedGprMdl.predict(Xtest, return_std=True) #(x, return_std=True)
"""
#END scikit

#GPy
windDirGprMdl.optimize_restarts(num_restarts=5, messages=True)
windSpeedGprMdl.optimize_restarts(num_restarts=5, messages=True)
currentDirGprMdl.optimize_restarts(num_restarts=5, messages=True)
currentSpeedGprMdl.optimize_restarts(num_restarts=5, messages=True)
depthGprMdl.optimize_restarts(num_restarts=5, messages=True)

windDirPred, windDirPredsigma = windDirGprMdl.predict(Xtest)
windSpeedPred, wspdsigma = windSpeedGprMdl.predict(Xtest)
currentDirPred, currentDirPredsigma = currentDirGprMdl.predict(Xtest)
currentSpeedPred, cspdsigma = currentSpeedGprMdl.predict(Xtest)
depthPred, depthsigma = depthGprMdl.predict(Xtest)
#print (windSpeedPred) #jm why is this NaN
#print depth_sensor_reading
#END GPy

# Plot
# Reshaping predictions in a grid form
grid_wind_dir_pred = np.reshape(windDirPred, LAT.shape);
windDirPredsigma = np.reshape(windDirPredsigma, LAT.shape)

grid_wind_speed_pred = np.reshape(windSpeedPred, LAT.shape);
wspdsigma = np.reshape(wspdsigma, LAT.shape)

grid_current_dir_pred = np.reshape(currentDirPred, LAT.shape);
currentDirPredsigma = np.reshape(currentDirPredsigma, LAT.shape)

grid_current_speed_pred = np.reshape(currentSpeedPred, LAT.shape);
cspdsigma = np.reshape(cspdsigma, LAT.shape)

grid_depth_pred = np.reshape(depthPred, LAT.shape);
depthsigma = np.reshape(depthsigma, LAT.shape)

# Calculate x and y components of the predicted wind speed.
grid_wind_x_vector = np.multiply(grid_wind_speed_pred, np.cos(grid_wind_dir_pred));
grid_wind_y_vector = np.multiply(grid_wind_speed_pred, np.sin(grid_wind_dir_pred));

# Calculate x and y components of the predicted current speed.
grid_current_x_vector = -np.abs(np.multiply(grid_current_speed_pred, np.cos(grid_current_dir_pred)))
grid_current_y_vector = -np.abs(np.multiply(grid_current_speed_pred, np.sin(grid_current_dir_pred)))

print (actual_current_speed)
#prepare uncertainty for instensity coloring on plot
"""
plt.figure()
exec('plt.quiver(lon' + TRAINING_DATA_RULE + ', lat' + TRAINING_DATA_RULE + ', actual_wind_speed_x' + TRAINING_DATA_RULE + ',  actual_wind_speed_y' + TRAINING_DATA_RULE + ')')

plt.plot(X[:,0], X[:,1], 'r+')
plt.title("Actual Wind Measurements")
plt.ticklabel_format(useOffset=False)

plt.figure()
plt.quiver(LON, LAT, grid_wind_x_vector, grid_wind_y_vector)
plt.plot(X[:,0], X[:,1], 'r+')
plt.title("Wind Intensity Map Grid")
plt.ticklabel_format(useOffset=False)
"""
plt.figure()
exec('plt.quiver(lon' + TRAINING_DATA_RULE + ', lat' + TRAINING_DATA_RULE + ', actual_current_speed_x' + TRAINING_DATA_RULE + ',  actual_current_speed_y' + TRAINING_DATA_RULE + ')')

plt.plot(X[:,0], X[:,1], 'r+')
plt.title("Actual Current Measurements")
plt.ticklabel_format(useOffset=False)

plt.figure()
plt.quiver(LON, LAT, grid_current_x_vector, grid_current_y_vector)
plt.plot(X[:,0], X[:,1], 'r+')
plt.title("Current Intensity Map Grid")
plt.ticklabel_format(useOffset=False)


def plot_contour(x, y, z, title, measurements=None):
    """  Plot contour with measurement points.
    Args:
        x: 1d vector
        y: 1d vector
        z: 1d vector
        measurements: 2d vector
        title: 1d vector
    """
    plt.figure()
    plt.contourf(x, y, z,linewidth=0)
    #if measurements is not None:
    #    plt.plot(measurements[:,0], measurements[:,1], 'r+')
    plt.colorbar(format = COLORBAR_FORMAT)
    plt.title(title)
    plt.ticklabel_format(useOffset=False)

plot_contour(LON, LAT, grid_depth_pred, "Depth Measurements", X)

plot_contour(LON, LAT, depthsigma, "Depth Uncertainty", X)

#plot_contour(LON, LAT, grid_wind_speed_pred, "Wind Speed Prediction", X)

#plot_contour(LON, LAT, wspdsigma, "Wind Speed Uncertainty", X)

#plot_contour(LON, LAT, grid_wind_dir_pred, "Wind Direction Prediction", X)

#plot_contour(LON, LAT, windDirPredsigma, "Wind Direction Uncertainty", X)

#plot_contour(LON, LAT, grid_current_speed_pred, "Current Speed Prediction", X)

#plot_contour(LON, LAT, cspdsigma, "Current Speed Uncertainty", X)

#plot_contour(LON, LAT, grid_current_dir_pred, "Current Direction Prediction", X)

#plot_contour(LON, LAT, currentDirPredsigma, "current Direction Uncertainty", X)


plt.show()
