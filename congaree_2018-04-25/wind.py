import matplotlib
import matplotlib.pyplot as plt

#from mpl_toolkits.basemap import Basemap
from mpl_toolkits.mplot3d import axes3d

import numpy as np
from numpy import ma

import pandas as pd
from pandas import DataFrame

import GPy

FONTSIZE = 30

# INPUT TO MODIFY
filename = 'data/congaree_2018-04-25.csv' # use this for whole data set
#filename = 'data/congaree_2018-04-25_depth_cleaned.csv' #use this for depth calcs omitting out of range.
#filename = 'data/congaree_2018-04-25_current_cleaned.csv' #use this for current calcs omitting 0 readings.
MEASUREMENT_STEP = 4 # Take measurements every MEASUREMENT_STEP
#1-1249: launching; 1250-2019: Dock parallel current sensors; 2020-2299: dock x-paralled, y-perpendicular; 2649-3059: autonomous wp mission single parraled; 4535-4630: auto perpendicular y, parralel x
SEQUENCE_MIN = 2649
SEQUENCE_MAX = 3100
TRAINING_DATA_RULE = "[0:len(pattern):MEASUREMENT_STEP]"
GRID_LATITUDE_NUM_CELLS = 50
GRID_LONGITUDE_NUM_CELLS = 50
VARIANCE_RBF_WIND_DIR = 0.1
LENGTHSCALE_RBF_WIND_DIR = 0.00001
VARIANCE_RBF_WIND_SPEED = 0.1
LENGTHSCALE_RBF_WIND_SPEED = 0.00001

# END INPUT TO MODIFY

# CONSTANTS
# Names of the fields in the CSV file
SEQUENCE_STRING = "seq"
LATITUDE_STRING = "Latitude"
LONGITUDE_STRING = "Longitude"
WIND_SENSOR_SPEED_STRING = "wind_sensor_speed"
WIND_SENSOR_DIR_STRING = "wind_sensor_dir"
COLORBAR_FORMAT = '%1.6f' # https://stackoverflow.com/questions/42153735/disabling-scientific-notation-of-imshow-colorbar-in-matplotlib
# END CONSTANTS

# DO NOT MODIFY UNLESS YOU KNOW WHAT YOU'RE DOING

# Read in data from CSV
readcsv = pd.read_csv(filename)
#Change sequence bounds to filter whole csv file for areas of interest.
pattern = readcsv.loc[(readcsv[SEQUENCE_STRING] > SEQUENCE_MIN) & (readcsv[SEQUENCE_STRING] < SEQUENCE_MAX)]
#pattern, test_data = train_test_split(readcsv, test_size = 0.25)

# Now the data is sliced in the region and sequence that we wanted.
# Here calculation of the wind vector, according to the transformations wrt boat and wind sensor measurement.

# Variables storing info from the CSV file
timestamp = readcsv.Timestamp

lat = pattern[LATITUDE_STRING]
lon = pattern[LONGITUDE_STRING]
wind_sensor_speed = pattern[WIND_SENSOR_SPEED_STRING] # m/s
wind_sensor_dir = np.radians(pattern[WIND_SENSOR_DIR_STRING]) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt boat reference frame

#convert boat heading in degrees to radians and read linear velocities
boat_heading = np.radians(pattern.boat_heading) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt World reference frame
boat_speed_linear_x = pattern.boat_linear_x # wrt boat reference frame TODO check
boat_speed_linear_y = pattern.boat_linear_y # wrt boat reference frame TODO check

#Some pre-processing to make knowns available in the correct format
#Convert Sensor Direction to World Reference Frame and convert to radians, ANGLEr
wind_sensor_dir_world = ((boat_heading+wind_sensor_dir)%(2*np.pi)) # In N (0) - E (pi/2) - W (3/2pi) - S (pi), wrt World reference frame
current_sensor_dir_world = boat_heading

# Transform the world reference frame to become from N (0) - E (pi/2) - W (3/2pi) - S (pi) to N (0) - E (3/2pi) - W (pi/2) - S (pi)
#Calc polar boat speed from x and y linear components, b
boat_speed_p = np.sqrt(np.power(boat_speed_linear_x, 2)+np.power(boat_speed_linear_y, 2))
#at this speed and boat heading calculate boat cartesian coordinates
boat_speed_x = np.abs(boat_speed_p) * np.cos(boat_heading)
boat_speed_y = -np.abs(boat_speed_p) * np.sin(boat_heading) # invert y axis to invert E and W angle 
#convert boat speed angle back to polar for Calculating Angle of real wind in system of equations
boat_heading_p = np.arctan2(boat_speed_y, boat_speed_x)

# Same calculations for the wind sensor.
#Calc x and y components of wind sensor velocity measurments Rx, Ry
wind_sensor_speed_x = -np.abs(wind_sensor_speed) * np.cos(wind_sensor_dir_world)
wind_sensor_speed_y = np.abs(wind_sensor_speed) * np.sin(wind_sensor_dir_world) #flipped cartesian y value
wind_sensor_dir_p = np.arctan2(wind_sensor_speed_y, wind_sensor_speed_x)

#Calculate the actual wind vector, starting from the following equation: wind_sensor = boat + actual_wind
#ANGLEw (wind direction in world ref frame)
actual_wind_dir = np.pi + np.arctan2((wind_sensor_speed_y - boat_speed_p * np.sin(boat_heading_p)), (boat_speed_p * np.cos(boat_heading_p) - wind_sensor_speed_x))
actual_wind_speed = np.abs((boat_speed_p * np.cos(boat_heading_p) - wind_sensor_speed_x) / np.cos(actual_wind_dir))
actual_wind_speed_x = np.abs(actual_wind_speed) * np.cos(actual_wind_dir)
actual_wind_speed_y = np.abs(actual_wind_speed) * np.sin(actual_wind_dir)

# Finished the calculation.


exec('X = pd.concat([lon' + TRAINING_DATA_RULE + ', lat' + TRAINING_DATA_RULE + '], axis=1).values')
exec('Y = pd.concat([actual_wind_dir' + TRAINING_DATA_RULE + ', actual_wind_speed' + TRAINING_DATA_RULE + '], axis=1).values')


#GPy
# Kernel creation
# RBF Kernel def
#gp_kernel_dir = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_WIND_DIR, lengthscale=LENGTHSCALE_RBF_WIND_DIR)
#gp_kernel_speed = GPy.kern.RBF(input_dim=2, variance=VARIANCE_RBF_WIND_SPEED, lengthscale=LENGTHSCALE_RBF_WIND_SPEED)

#Matern32 Kernel def
gp_kernel_speed = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_WIND_SPEED, lengthscale=LENGTHSCALE_RBF_WIND_SPEED)
gp_kernel_dir = GPy.kern.Matern32(input_dim=2, variance=VARIANCE_RBF_WIND_DIR, lengthscale=LENGTHSCALE_RBF_WIND_DIR)

windDirGprMdl = GPy.models.GPRegression(X, np.reshape(Y[:,0],(len(Y[:,0]),1)),gp_kernel_dir, noise_var=0.1 )
windSpeedGprMdl = GPy.models.GPRegression(X,  np.reshape(Y[:,1],(len(Y[:,1]),1)),gp_kernel_speed, noise_var=0.1 )
#END GPy

#Test set over a grid.
# Create the grid and the test dataset.
grid_longitude = np.linspace(min(X[:,0]), max(X[:,0]), GRID_LONGITUDE_NUM_CELLS) #50 jm
grid_latitude = np.linspace(min(X[:,1]), max(X[:,1]), GRID_LATITUDE_NUM_CELLS) #50 jm

LON, LAT = np.meshgrid(grid_longitude, grid_latitude)
Xtest = np.array([LON.flatten(), LAT.flatten()]).T


#GPy
windDirGprMdl.optimize_restarts(num_restarts=5, messages=True)
windSpeedGprMdl.optimize_restarts(num_restarts=5, messages=True)

windDirPred, windDirPredsigma = windDirGprMdl.predict(Xtest)
windSpeedPred, wspdsigma = windSpeedGprMdl.predict(Xtest)
#print (windSpeedPred) #jm why is this NaN
#END GPy

# Plot
# Reshaping predictions in a grid form
grid_wind_dir_pred = np.reshape(windDirPred, LAT.shape);
windDirPredsigma = np.reshape(windDirPredsigma, LAT.shape)

grid_wind_speed_pred = np.reshape(windSpeedPred, LAT.shape);
wspdsigma = np.reshape(wspdsigma, LAT.shape)

# Calculate x and y components of the predicted wind speed.
grid_wind_x_vector = np.multiply(grid_wind_speed_pred, np.cos(grid_wind_dir_pred));
grid_wind_y_vector = np.multiply(grid_wind_speed_pred, np.sin(grid_wind_dir_pred));

#prepare uncertainty for instensity coloring on plot

plt.figure()
exec('plt.quiver(lon' + TRAINING_DATA_RULE + ', lat' + TRAINING_DATA_RULE + ', actual_wind_speed_x' + TRAINING_DATA_RULE + ',  actual_wind_speed_y' + TRAINING_DATA_RULE + ')')

plt.plot(X[:,0], X[:,1], 'r+')
plt.title("Actual Wind Measurements", fontsize=FONTSIZE)
plt.ticklabel_format(useOffset=False)
xlabel = "Longitude"
ylabel = "Latitude"
plt.xlabel(xlabel, fontsize=FONTSIZE)
plt.ylabel(ylabel, labelpad = 15, fontsize=FONTSIZE)
plt.subplots_adjust(left=0.2)
ax = plt.gca()
ax.tick_params(axis = 'both', which = 'major', labelsize=FONTSIZE)
ax.tick_params(axis = 'both', which = 'minor', labelsize=FONTSIZE)
"""
plt.figure()
plt.quiver(LON, LAT, grid_wind_x_vector, grid_wind_y_vector)
plt.plot(X[:,0], X[:,1], 'r+')
plt.title("Wind Intensity Map Grid")
plt.ticklabel_format(useOffset=False)
"""
def plot_contour(x, y, z, title, measurements=None):
    """  Plot contour with measurement points.
    Args:
        x: 1d vector
        y: 1d vector
        z: 1d vector
        measurements: 2d vector
        title: 1d vector
    """
    plt.figure()
    plt.contourf(x, y, z,linewidth=0)
    #if measurements is not None:
    #    plt.plot(measurements[:,0], measurements[:,1], 'r+')
    cbar = plt.colorbar(format = COLORBAR_FORMAT)
    plt.title(title, fontsize=FONTSIZE)
    plt.ticklabel_format(useOffset=False)
    ax = plt.gca()
    ax.tick_params(axis = 'both', which = 'major', labelsize=FONTSIZE)
    ax.tick_params(axis = 'both', which = 'minor', labelsize=FONTSIZE)
    cbar.ax.tick_params(labelsize=FONTSIZE)
    
'''
plot_contour(LON, LAT, grid_wind_speed_pred, "Wind Speed Prediction", X)

plot_contour(LON, LAT, wspdsigma, "Wind Speed Uncertainty", X)

plot_contour(LON, LAT, grid_wind_dir_pred, "Wind Direction Prediction", X)

plot_contour(LON, LAT, windDirPredsigma, "Wind Direction Uncertainty", X)
'''

plt.show()
